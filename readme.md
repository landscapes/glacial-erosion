# Forming Terrains by glacial erosion

Guillaume Cordonnier, Guillaume Jouvet, Adrien Peytavie, Jean Braun, Marie-Paule Cani, Bedrich Benes, Eric Galin, Eric Guérin, James Gain

| [Paper](http://www-sop.inria.fr/reves/Basilic/2023/CJPBCBGGG23/Sigg23_Glacial_Erosion__author.pdf) | [Video](https://www.youtube.com/watch?v=xfk_J4VhdWA) |

This repository contains the code for the paper *Forming Terrains by glacial erosion*. Further research on glacier flow modeling and glacier erosion will be maintained in the [IGM](https://github.com/jouvetg/igm2) repository. 

## Instalation
The code is self-contained and only depends on `numpy`, `pytorch` and `tqdm`. It was tested with numpy 1.19.2 and pytorch 1.13.0.

## Usage
The file `run_erosion.py` gives an exemple on how to use the different components of our method. The repository contains an input terrain at 64m resolution (`data/bedrock_64.npy`) and a pre-trained network for that resolution and 9 velocity layers (`data/net/net_9_5_64_6_0_64.pth`). The training is usualy done on-the-fly, pre-training is not completely necessary but it avoid large oscilations at the begining of the simulation, and is usually done by progressivley lowering the climate on the input terrain once without erosion for a glacial cycle (10000 iterations at dt = 10 years). The pre-network can then be used on similar terrains.

Note that this code uses a re-implementation of the GPU version of the fluvial erosion described in [[Schott 2023](https://hal.science/hal-04049125/document)] instead of our previous CPU version described in the paper: at the time scales involved for glacial erosion, we did not notice any difference and this makes the code more self-contained.

## Bibtex

```
@article{cordonnier2023glacialerosion,
    author = {Cordonnier, Guillaume and Jouvet, Guillaume and Peytavie, Adrien and Braun, Jean and Cani, Marie-Paule and Benes, Bedrich and Galin, Eric and Gu\'{e}rin, Eric and Gain, James},
    title = {Forming Terrains by Glacial Erosion},
    year = {2023},
    volume = {42},
    number = {4},
    articleno = {61},
    numpages = {14},
}
```

## Acknowledgements and Funding
This project was sponsored by the Agence Nationale de la Recherche project Invterra ANR-22-CE33-0012-01 to Cordonnier, by the USDA NIFA, Award \#2023-68012-38992 grant ``Promoting Economic Resilience and Sustainability of the Eastern U.S. Forests'' to Benes, by Hi!Paris's fellowship on Creative AI to Cani, by the National Research Foundation of South Africa (Grant Number 129257) to Gain, and the Agence Nationale de la Recherche project Ampli ANR-20-CE23-0001.



