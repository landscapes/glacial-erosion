import numpy as np

import torch
import tqdm

import glacier.blatter      as blatter
import glacier.mesh         as gmesh
import glacier.network      as network
import glacier.mass_balance as mass_balance
import glacier.transport    as transport
import glacier.gpu_erosion  as erosion


device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')


def main():

    # bedrock
    z = torch.tensor(np.load('data/bedrock_64.npy'))[None].to(device)

    #ice
    h = torch.zeros_like(z)

    # parameters
    dx = 64
    dt_max_hires = .1
    b_constants = blatter.BlatterConstants(78, 3, 15, 1/2)
    cyclic_bc = 'None' # or 'X' or 'Y'

    def on_create_network(network, **kwargs):
        network.load('data/net', kwargs['dx'])
        network.set_optimizer(torch.optim.Adam(network.parameters(), lr=1e-5, ))

    net = network.network_manager.get(vel_layers = 9, cyclic_bc=cyclic_bc, device = z.device, on_create = on_create_network, on_create_args = {'dx' : dx})
    save_network = False

    #erosion data
    bound = torch.ones_like(z, dtype = bool)
    if cyclic_bc == 'None':
        bound[:, 1:-1, 1:-1] = 0
    elif cyclic_bc == 'X':
        bound[:, 1:-1, :] = 0
    elif cyclic_bc == 'Y':
        bound[:, :, 1:-1] = 0
    else:
        raise NameError(f'cyclic_bc = {cyclic_bc}')
    
    flux = torch.zeros_like(z)
    lake = erosion.init_lake(z, bound, cyclic_bc)-z

    #generate water surface:
    water_surface = z + lake
    for _ in range(500):
        water_surface = erosion.lake_step(z, water_surface, bound, cyclic_bc, eps = 1e-7) 
    lake = water_surface-z

    noise = torch.rand_like(z)

    uplift = .5e-3

    niter = 10 * (100 + 40)
    for i in tqdm.tqdm(range(niter)):

        # climate:
        ELA = 2000-min(i/1000, 1)*800 +max(i-1000, 0)/400*800
        # reduce dt to better capture the fast warming
         
        dt = 10 if niter < 1000 else 5
        dt_erosion = dt

        # Mass balance
        M =  mass_balance.simple_mass_balance(z+h, ELA, gradabl = .009, gradacc = .005)
        M += mass_balance.sun_modifier(z+h, dx, cyclic_bc)
        M = M.clamp(max = 2)

         # compute mesh
        mesh = gmesh.Mesh(z, z+h, dx, 20, 9, cyclic_bc, False)

        # get velocity and update network
        u = net.get_velocity(
            z, h, b_constants.get_c(z), dx,
            frames_before_retrain = 1,
            energy_function = lambda u : blatter.blatter_energy(u, mesh, b_constants)
        ).detach()

        # basal velocity
        ub = mesh.ubase(u)
        # vertically averaged velocity
        u  = mesh.ubar(u)

        if save_network:
            net.save("data/net", dx, frames_before_save = 100)

        # transport
        h = transport.transport(u, h, M, dx, dt, cfl = .1, dt_max_hires = dt_max_hires, cyclic_bc = cyclic_bc, transport_type = "multiscale_adapt")

        # erosion
        ice_surface_slope = erosion.downward_slope_squared(z+h, dx, cyclic_bc)**.5
        inactive_ice_mask   = (ice_surface_slope > .7) | (h<1)
        inactive_ice_mask = h<1
        visible_ice       = (h - (ice_surface_slope - .7).clamp(0)*dx).clamp(0)

        water_surface = erosion.lake_step(z, z + lake, bound, cyclic_bc, eps = 1e-7) 
        lake = water_surface-z

        z += (~bound) * uplift*dt_erosion

        z = erosion.glacial_abrasion(z, ub, dx, dt_erosion, cyclic_bc, (~inactive_ice_mask) * 2.7e-6)   # * [.5-2]
        z = erosion.glacial_quarry  (z, ub, dx, dt_erosion, cyclic_bc, (~inactive_ice_mask) * 6.5e-4 *2) # * [.5-4]

        flux, norm = erosion.flux_step(water_surface, flux, 1, dx, cyclic_bc)    
        sl  =  erosion.downward_slope_squared(water_surface, 1, cyclic_bc)[0]**.5

        z += (~bound) * dt_erosion * erosion.hillslope(z, inactive_ice_mask * 10e-2, dx, cyclic_bc) # irock(2e-2, 10e-2)
        # SPL
        z = erosion.SPL(z, sl/dx, flux/norm*sl, inactive_ice_mask * 2 * noise * 1.6e-5, .4, 1,  0, dx, dt_erosion) #(1.0e-5, 1.6e-5)
        # debris flow
        z = erosion.SPL(z, sl/dx, flux/norm*sl, inactive_ice_mask * 2 * noise * 2.56e-5, .8, 2, .57, dx, dt_erosion) #irock(   1.5e-5,  3.7e-5)
        
        lake = erosion.lake_z_offset(water_surface, z, bound)-z

    # output
    np.save('data/bedrock_64_eroded', z.cpu()[0])


if __name__ == "__main__":
    main()