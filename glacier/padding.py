import torch

def reflect_pad(s, cyclic_bound, n = 1):

    if cyclic_bound != 'X':
        s = torch.cat((
            torch.flip(s[..., :, :n], (-1,)), 
            s, 
            torch.flip(s[..., :, -n:], (-1,))), dim  = -1)

    if cyclic_bound != 'Y':
        s = torch.cat((
            torch.flip(s[..., :n, :], (-2,)), 
            s, 
            torch.flip(s[..., -n:, :], (-2,))), dim = -2)
        
    if cyclic_bound == 'X':
        s = torch.cat((s[..., :, -n:], s, s[..., :, :n]), dim  = -1)

    if cyclic_bound == 'Y':
        s = torch.cat((s[..., -n:, :], s, s[..., :n, :]), dim  = -2)

    return s

def constant_pad(s, cyclic_bound, c = 0, n = 1):

    if cyclic_bound != 'X':
        s = torch.cat((
            c + 0*s[..., :, :n], 
            s, 
            c + 0*s[..., :, -n:]), dim  = -1)

    if cyclic_bound != 'Y':
        s = torch.cat((
            c + 0*s[..., :n, :], 
            s, 
            c + 0*s[..., -n:, :]), dim = -2)
        
    if cyclic_bound == 'X':
        s = torch.cat((s[..., :, -n:], s, s[..., :, :n]), dim  = -1)

    if cyclic_bound == 'Y':
        s = torch.cat((s[..., -n:, :], s, s[..., :n, :]), dim  = -2)

    return s


def conditional_pad(s, cyclic_bound, n = 1):

    """
    Change the shape and apply padding on the left / top only if cyclic boundaries are required
    """

    if cyclic_bound == 'X':
        s = torch.cat((s, s[..., :, :n]), dim  = -1)

    if cyclic_bound == 'Y':
        s = torch.cat((s, s[..., :n, :]), dim  = -2)

    return s