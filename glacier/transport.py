import math
import torch
import torch.nn.functional as F
from . import multires
from .padding import reflect_pad, constant_pad


def transport(u, h, M, dx, dt, cfl, dt_max_hires, cyclic_bc, transport_type = "multiscale_adapt"):
    """
    Generic transport function, that inplement different algorithms depending on `transport_type`:
        transport_type = "FVM": classical finite volume, constant dt
        transport_type = "adapt": classical finite volume but with adaptive dt; the dt is allowed tochange depending on each cell's stability condition
        transport_type = "multiscale_adapt": the time scale is also adapted to the scales, to have a better approximation of a transport by timestep dt

    Input:
        u: velocity
        h: ice thickness
        M: mass balance (source term)
        dx: dx
        dt: target timestep (only used for "multiscale_adapt" )
        cfl: cfl constant
        dt_max_hires: max timestep allowed at high resolution
        transport_type: see above

    returns:
        - transported ice
        - dt: the minimum timestep at coarsest scale (ideally should be esual to dt for the multiscale_adapt case, otherwise this is the minimum dt allowed by  the cfl)

    """

    # pad the (cell-centered velocities)
    u = reflect_pad(u, cyclic_bc)

    # compute staggered velocities
    ux = .5 * (u[0:1, :, 1:] + u[0:1, :, :-1]) # h, w-1
    uy = .5 * (u[1:2, 1:, :] + u[1:2, :-1, :]) # h-1, w

    # compute maximum time step
    dt_cfl = dt_transport_adapt(ux, uy, dx, cfl, dt_max_hires)

    if cyclic_bc != 'X':
        dt_cfl[..., :, [0, -1]]*=.5 # stabilize the bounds

    if cyclic_bc != 'Y':
        dt_cfl[..., [0, -1], :]*=.5

    out_dt = dt_cfl.max()

    if transport_type == "FVM":
        dt_cfl = out_dt #for FVM the timestep is constant

    #compute right hand side: M - div uh
    div_uh = transport_div(ux, uy, h, dx, cyclic_bc)
    rhs = (M-div_uh)

    # rhs is not defined outside of the glacier, replace by 0
    bound = (h==0) & (rhs<=0)
    rhs = torch.where(bound, torch.zeros_like(rhs), rhs)

    # estimate dt * rhs 
    if transport_type == 'FVM' or transport_type == 'adapt':
        dt_rhs = dt_cfl * rhs
        
    else: #if transport_type == 'multiscale_adapt'
        dt_rhs, out_dt = residual_rhs(rhs, dt, dt_cfl, 7, cyclic_bc)
        
    # update h
    return torch.where(bound, torch.zeros_like(h), h + dt_rhs).clamp(0)


def dt_transport_adapt(ux, uy, dx, cfl, dt_max):
    """
    Per-cell maximum time step to respect the CFL condition

    Input:
        vx, vy: staggered velocity
        cfl: stability constant
        dt_max: max allowed time step

    Output:
        tensor of maximum timesteps (per node)
    """

    # maximum velocity per cell
    ux, uy = ux.abs(), uy.abs()
    max_ux = torch.maximum(ux[:, 1:-1, :-1],  ux[:, 1:-1, 1:])
    max_uy = torch.maximum(uy[:, :-1, 1:-1],  uy[:, 1:, 1:-1])
    max_u = torch.maximum(max_ux, max_uy)

    #maximum time step
    return 1 / torch.maximum(max_u/cfl/dx, torch.tensor(1/dt_max))


def transport_div(ux, uy, s, dx, cyclic_bc):
    """
    Compute the divergence term `div (u s)` in the transport equation 
    Input:
        ux, uy: staggered velocity
        s: quantity to transport
        dx: dx
    """

    # select s upwind
    s = constant_pad(s, cyclic_bc)
    s_x = torch.where(ux > 0, s[:, :, :-1], s[:,  :, 1:])
    s_y = torch.where(uy > 0, s[:, :-1, :], s[:, 1:,  :])
    
    # flux
    usx = (ux*s_x)[..., 1:-1, :] #h, w+1
    usy = (uy*s_y)[..., 1:-1]
            
    # compute div (u s)
    dusx = (usx[:, :, 1:] - usx[:, :, :-1])/dx #h, w
    dusy = (usy[:, 1:, :] - usy[:, :-1, :])/dx #h, w

    return dusx + dusy


def residual_rhs(rhs, dt, dt_max_hr, num_levels, cyclic_bc, sigma = 1.0, window_size = 3):
    """
    Multiscale adaptive transport

    Input:
        rhs: the right hand side of the transport equation (M-div uh)
        dt:  target dt
        dt_max_hr: maximum stable dt at fine resolution (computed by dt_transport_adapt)
        num_levels: number of scales
        sigma, window_size: gaussian parameters used in the upsampling

    returns:
        - approximation of dt*div uh with stability ensure at all scales
        - minimum timestep at coarsest scale
    """

    num_levels = min(num_levels, int(math.log2(min(rhs.shape[-2:]))))

    # compute residuals (note that the residuals are upsampled: all layers have the same resolution but represent different scales of the signal)
    res = multires.GResiduals(sigma, window_size, cyclic_bc).to(rhs.device)
    r_rhs  = torch.cat(res(rhs[None], num_levels), dim = 0)
    # dt per scale
    dt = torch.clamp(dt_max_hr[None]*1.8**torch.arange(num_levels+1, device = rhs.device)[:, None, None, None], max = dt)

    # sum the residuals multiplied by the corresponding time step
    return torch.sum(r_rhs * dt, dim = 0), dt[-1].min()