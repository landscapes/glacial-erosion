import torch
import torch.nn.functional as F

from .padding import reflect_pad



##############################################################
##################### Lakes ##################################
##############################################################

def init_lake(z, bounds, cyclic_bc):
    """
    It is easier to keep track of the lakes if we assume no local minima in the inital klake surface.
    This fuction create such a surface by computing a cone over the maximum height of the terrain
    
    Input:
        z: heightmap
        bounds: boundary mask

    Output:
        lake surface intial guess
    """

    m = z.max()+1e-3
    X = torch.linspace(-1, 1, z.shape[-1], device = z.device)**2 * (cyclic_bc != 'X')
    Y = torch.linspace(-1, 1, z.shape[-2], device = z.device)**2 * (cyclic_bc != 'Y')

    return torch.where(bounds, z, m + 1.8-torch.sqrt(X.reshape(1, 1, z.shape[-1]) + Y.reshape(1, z.shape[-2], 1)))

def lake_step(z, s, bounds, cyclic_bc, eps = 1e-3):

    """
    Reduce the difference between the previously estimated lake surface and the ground truth 
    
    Input:
        z: bedrock elevation
        s: lake surface
        
    Output:
        improved lake surface
    """

    s = reflect_pad(s, cyclic_bc)


    min_nb = torch.minimum(
        torch.minimum(s[:, 1:-1, 2:], s[:, 1:-1, :-2]),
        torch.minimum(s[:, 2:, 1:-1], s[:, :-2, 1:-1])
    )

    s_min = min_nb + eps

    return torch.where(bounds, z, torch.maximum(s_min, z))

def lake_z_offset(s, new_z, bounds):
    return torch.where(bounds, s, s + (new_z-s).clamp(0).max())


def flux_step(s, Q, p, dx, cyclic_bc):

    """
    Improves the estimation of the water draiange (or flux)
    
    Input:
        s: surface elevation
        Q: flux estimation
        p: precipitation map
        dx: dx
        
    Output:
        - improved flux
        - sum of receiver height difference, tio be used as a flux correction term
    """

    # for boundaries
    s = reflect_pad(s, cyclic_bc)
    Q = reflect_pad(Q, cyclic_bc)

    rcv_norm =  reflect_pad(
        (-s[:, 2:, 1:-1]  + s[:, 1:-1, 1:-1]).clamp(0)+ 
        (-s[:, :-2, 1:-1] + s[:, 1:-1, 1:-1]).clamp(0)+ 
        (-s[:, 1:-1, 2:]  + s[:, 1:-1, 1:-1]).clamp(0)+ 
        (-s[:, 1:-1, :-2] + s[:, 1:-1, 1:-1]).clamp(0),
        cyclic_bc
    )

    rcv_norm += (rcv_norm==0)

    Q = Q/rcv_norm

    donnor_contrib = (
        (s[:, 2:, 1:-1]  - s[:, 1:-1, 1:-1]).clamp(0) * Q[:, 2:, 1:-1] + 
        (s[:, :-2, 1:-1] - s[:, 1:-1, 1:-1]).clamp(0) * Q[:, :-2, 1:-1] + 
        (s[:, 1:-1, 2:]  - s[:, 1:-1, 1:-1]).clamp(0) * Q[:, 1:-1, 2:] + 
        (s[:, 1:-1, :-2] - s[:, 1:-1, 1:-1]).clamp(0) * Q[:, 1:-1, :-2]
    )

    return p*dx*dx + donnor_contrib, rcv_norm[..., 1:-1, 1:-1]


def downward_grad(z, dx, cyclic_bc):
    """
    compute the x and y gradients toward the steepes slope in both x and y directions
    """
    z = reflect_pad(z, cyclic_bc)
    sx = (z[..., 1:] - z[..., :-1])/dx
    sy = (z[..., 1:, :] - z[..., :-1, :])/dx
    sx = torch.maximum(sx[..., 1:-1, :-1].clamp(0), (-sx[..., 1:-1, 1:]).clamp(0))
    sy = torch.maximum(sy[..., :-1,1:-1].clamp(0), (-sy[...,1:, 1:-1]).clamp(0))
    return sx, sy

def max_grad(z, dx):
    """
    compute gradients (oriented downind or upwind) 
    """
    sx = (z[..., 1:] - z[..., :-1])/dx
    sy = (z[..., 1:, :] - z[..., :-1, :])/dx
    msx = torch.where(sx[..., 1:].abs() > sx[..., :-1].abs(), sx[..., 1:], sx[..., :-1])
    msy = torch.where(sy[..., 1:, :].abs() > sy[..., :-1, :].abs(), sy[..., 1:, :], sy[..., :-1, :])

    return (torch.cat([sx[...,[0]], msx, sx[...,[-1]]], dim = -1),
            torch.cat([sy[...,[0],:], msy, sy[...,[-1],:]], dim = -2))

def downward_slope_squared(z, dx, cyclic_bc):
    """
    norm of the downward gradient
    """
    sx, sy = downward_grad(z, dx, cyclic_bc)
    return sx**2+sy**2




def SPL(z, slope, drain, k, m, n, cs, dx, dt):
    """
    Implementation of the explicit stream power law
    
    Input:
        z: surface elevation
        slope: norm of downward slope
        drain: (corrected) water flux
        k, m, : SPL erosion constants
        cs: critical slope above which the erosion takes place (for debris flow)
        dx, dt: dx, dt
        
    Output:
        - eroded surface
    """

    factor = (dt * k *drain**m).clamp(max = .5*dx) #clamp to avoid instabilities
    e = factor*(slope-cs).clamp(0)**n

    return z - e


def hillslope(z, k, dx, cyclic_bc):
    """
    Implementation of the explicit hillslope erosion
    
    Input:
        z: surface elevation
        k: hillslope erosion constants
        dx, dt: dx, dt
        
    Output:
        - eroded amount per unit time
    """

    z = reflect_pad(z, cyclic_bc)
    k = reflect_pad(k, cyclic_bc)

    q_x = (z[..., 1:] - z[..., :-1])/dx * (k[..., 1:] + k[..., :-1])/2
    q_y = (z[..., 1:, :] - z[..., :-1, :])/dx * (k[..., 1:, :] + k[..., :-1, :])/2
    Dx = (q_x[..., 1:-1, 1:] - q_x[..., 1:-1, :-1])/dx
    Dy = (q_y[..., 1:, 1:-1] - q_y[..., :-1, 1:-1])/dx

    D = Dx+Dy

    return D


def glacial_abrasion(z, v, dx, dt, cyclic_bc, k_erosion = 2.7e-7, l = 2.02):
    """
    Implementation of the glacial abrasion
    
    Input:
        z: surface elevation
        v: cell-centered basal velocity
        dx, dt: dx, dt
        k, l: abrasion erosion constants (default are from Herman 2015 Science paper)
        
    Output:
        - eroded surface
    """

    # velocity norm
    vn = torch.clamp(torch.sqrt(torch.sum(v**2, dim = 0)), max = 10000)

    e = (k_erosion*vn**l).clamp(max = 1)
    s2 = downward_slope_squared(z, dx, cyclic_bc)

    return z - dt * e * torch.sqrt(1 + s2) #high slope correction term 

def glacial_quarry(z, v, dx, dt, cyclic_bc, k_erosion = 2.6e-6):
    """
    Implementation of the glacial quarying
    
    Input:
        z: surface elevation
        v: cell-centered basal velocity
        dx, dt: dx, dt
        k_erosion: quarying erosion constany
        
    Output:
        - eroded surface
    """

    sx, sy = max_grad(z, dx)

    vn = torch.sqrt(torch.sum(v**2, dim = 0))
    beta = -(sx*v[0] + sy*v[1])/(vn+1e-3)
    gamma = torch.erf(beta/.4)*.5+.5

    e = (k_erosion*vn*gamma).clamp(max = 1)
    s2 = downward_slope_squared(z, dx, cyclic_bc)
    return z - dt * e * torch.sqrt(1 + s2)