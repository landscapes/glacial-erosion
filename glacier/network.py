import torch
import torch.nn as nn
import torch.nn.functional as F

from . import multires
from . import mesh 
from .padding import reflect_pad

import os

class CyclicConv2d(nn.Module):

    def __init__(self, in_channels, out_channels, kernel_size, padding, cyclic_bc):
        super().__init__()
        self.conv = nn.Conv2d(in_channels, out_channels, kernel_size, padding = 0)
        self.padding = padding
        self.cyclic_bc = cyclic_bc

    def forward(self, x):
        return self.conv(reflect_pad(x, self.cyclic_bc, self.padding))

def VelConv(in_channels, out_channels, kernel_size, cyclic_bc):
    """
    Helper convolution function
    """
    return nn.Sequential(
        CyclicConv2d(in_channels, out_channels, kernel_size, padding=(kernel_size-1)//2, cyclic_bc = cyclic_bc),
        nn.ReLU()
    )

def make_input(_dx, z, h, c, cyclic_bc):

    """
    Constructs input from geometry, outputs a stack of h + gradient toward each 4 directions + sliding coeficient
    """
    
    s = reflect_pad(h+z, cyclic_bc)
    h = reflect_pad(h, cyclic_bc)
    c = reflect_pad(c, cyclic_bc)
    
    
    sx = mesh.dx(s) / _dx
    sy = mesh.dy(s) / _dx

    return torch.cat(
        [h[:, :, 1:-1, 1:-1]/500, 
         sx[:, :, 1:-1, 1:], sx[:, :, 1:-1, :-1], sy[:, :, 1:, 1:-1], sy[:, :, :-1, 1:-1], 
         c[:, :, 1:-1, 1:-1] ], dim = 1)


def make_output(y, h, num_layers):

    """
    Convert network output to velocities
    """

    #100 is a scaling factor for the velocities
    return 100 * y.reshape(-1, 2, num_layers, y.shape[2], y.shape[3])


def network_tag(vel_layers, pyramid_size = 5, CNN_features = 64, CNN_layers = 6, MLP_layers = 0):
    """
    tag to identify the architecture
    """
    return '_'.join(str(i) for i in [vel_layers, pyramid_size, CNN_features, CNN_layers, MLP_layers])

class Network(nn.Module):

    def __init__(self, vel_layers, cyclic_bc, pyramid_size = 5, CNN_features = 64, CNN_layers = 6, MLP_layers = 0):
        super().__init__()

        self.tag = network_tag(vel_layers, pyramid_size, CNN_features, CNN_layers, MLP_layers)

        self.param_pyramid_size = pyramid_size
        self.param_conv_layers = [CNN_features]*(CNN_layers + MLP_layers)
        self.param_conv_kernels = [3]*CNN_layers + [1]*MLP_layers

        self.cyclic_bc = cyclic_bc
        self.num_layers = vel_layers
        self.pyramid = multires.GPyramid(1, 5, cyclic_bc)
               

        self.param_conv_layers = [6*(1+self.param_pyramid_size)] + self.param_conv_layers
        self.convs = nn.Sequential(*[
            VelConv(self.param_conv_layers[i], self.param_conv_layers[i+1], self.param_conv_kernels[i], cyclic_bc) for i in range(len(self.param_conv_layers)-1)
        ])

        self.linear = nn.Conv2d(self.param_conv_layers[-1], 2*self.num_layers, kernel_size=1)
        
        self.total_iters = 0

    def forward(self, z, h, c, _dx):

        x = make_input(_dx, z, h, c, self.cyclic_bc) * (h > 1e-5)
        input = torch.cat(self.pyramid(x, self.param_pyramid_size), dim = 1)
        return make_output(self.linear(self.convs(input)), h, self.num_layers)
    

    def set_optimizer(self, opt):
        """
        Because we use the network on a interactive framework, we want to keep the optimizer with the network
        """
        self.optimizer = opt

    def get_velocity(self, z, h, c, _dx, frames_before_retrain = 0, energy_function = None):
        """
        Get the velocity and optionaly retrain

        Input:
            z: bedrock
            h: ice thickness
            c: sliding constant
            _dx : dx
            frames_before_retrain: number of frame to wait before retraining the network. 0 means do not retrain, 1 meands every frame
            energy_function: function f(u) used to evaluate the energy 
        """

        if torch.all(h==0):
            return torch.zeros((2, self.num_layers) + z.shape[-2:], device = z.device)

        do_train = frames_before_retrain != 0 and self.total_iters % frames_before_retrain == 0
        self.total_iters += 1

        if do_train:

            if not hasattr(self, 'optimizer'):
                raise AttributeError("Network training required without an optimizer. Pelase attach an optimizer to the network first with `set_optimizer`, or disable training with the argument `frames_before_retrain = 0`")

            if energy_function is None:
                raise AttributeError("frames_before_retrain is not 0 but no energy function was provided")
        
            self.optimizer.zero_grad()
        
        u = self(z[None], h[None], c[None], _dx)[0]

        if do_train:
            loss = energy_function(u)
            loss.backward()
            self.optimizer.step()

        return u
    
    def save(self, folder, dx, frames_before_save = 1):
        if frames_before_save != 0 and self.total_iters % frames_before_save == 0:
            path = os.path.join(folder, f'net_{self.tag}_{int(dx)}.pth')
            torch.save(self.state_dict(), path)
        
    def load(self, folder, dx):
        path = os.path.join(folder, f'net_{self.tag}_{int(dx)}.pth')
        self.load_state_dict(torch.load(path))



        
class _NetworkManager:
    """
    Manager to keep the network in an interactive interface
    """

    def __init__(self):
        self.networks = {}

    def get(self, vel_layers, cyclic_bc, pyramid_size = 5, CNN_features = 64, CNN_layers = 6, MLP_layers = 0, device = 'cpu', on_create = None, on_create_args = None):
        
        # cyclic_bc is not on tag because it does not change the architecture
        tag = network_tag(vel_layers, pyramid_size, CNN_features, CNN_layers, MLP_layers)

        if tag not in self.networks:
            self.networks[tag] = Network(vel_layers, cyclic_bc, pyramid_size, CNN_features, CNN_layers, MLP_layers).to(device)

            if on_create is not None:
                on_create_args = {} if on_create_args is None else on_create_args
                on_create(self.networks[tag], **on_create_args)
        
        return self.networks[tag]
        

network_manager = _NetworkManager()