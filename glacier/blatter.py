import math
import torch

from . import mesh as gmesh


class BlatterConstants:

    """
    Storage class for all constants used during the simulation
    Handles the conversion from A, n  to B, p convention
    """

    def __init__(self, A, n, c, m, rho = 917, g = 9.8):
        # A in MPa^-3 a^-1 (TODO: n!=3)
        A = A * 1e-18
        p = (n+1)/n
        B = (A/math.sqrt(2)**(n-1))**(-1/n)

        # C = Pa (m.a^-1)^-m
        #  tau = C u^m  => C = c^-m
        # c in Pa^-1/m m.a^-1

        # c = c*1e6**(-1/m)*1e3
        # compute  1/C
        # inv_C = 1e-6 * (1e3*c)**m
        inv_C = (c * 1e-10)**m
        s = m+1

        self.B, self.p, self.s, self.rho_g = B, p, s, rho*g
        self.c = c

        # C holds for both sliding and non sliding bc (inv_C = 0, and can be given as a map of shape (1,H, W))
        if hasattr(inv_C, 'shape'):
            # frozen collocated with velocities
            self.frozen = inv_C == 0 
            # but C collocated with integral sampling
            self.C = 1/gmesh.hz_avg(inv_C).clamp((1e-10)**(s-1)) #clamp(1e-6 * (1e3*.1)**(s-1))   #clamp at c = . #TODO clamping condition for inv_C
        else:
            self.frozen = inv_C == 0
            self.C = 1/max(inv_C, 1e-6 * (1e3*.1)**(s-1))

    def get_c(self, like):
        if hasattr(self.c, 'shape'):
             return self.c
        else:
             return torch.full_like(like, self.c)
         


def blatter_energy(u, mesh, constants):
        
        """
        Evaluates the energy from Baltter approximation
        Input:
            u: layers of velocity
            mesh: discretization
            constants: physical constants
        """
        
        # values and derivatives of the velocity
        sampling = mesh.sample_velocity(u, constants.frozen)
        
        #loss scale to keep reasonable values
        loss_scale = .5/constants.rho_g / mesh.total_volume * (~mesh.empty_cell).sum()/8

        # strain rate tensor
        D = torch.stack((
            sampling.cell_dudx[0], .5*(sampling.cell_dudy[0] + sampling.cell_dudx[1]), .5*sampling.cell_dudz[0],
            .5*(sampling.cell_dudy[0] + sampling.cell_dudx[1]), sampling.cell_dudy[1], .5*sampling.cell_dudz[1],
            .5*sampling.cell_dudz[0], .5*sampling.cell_dudz[1], -sampling.cell_dudx[0] -sampling.cell_dudy[1]
        ), dim = -1)
        
        norm_strain_2 = 1e-6+torch.sum(D*D, dim = -1)

        # viscosity energy
        viscosity_factor = constants.B * loss_scale
        viscosity = viscosity_factor / constants.p * torch.sum(mesh.cell_volume[0]* norm_strain_2**(constants.p/2))

        # gravity energy
        gravity_term = sampling.cell_u[0] * mesh.surface_grad[0, None] + sampling.cell_u[1] * mesh.surface_grad[1, None]
        gravity_factor = constants.rho_g * loss_scale
        gravity   = gravity_factor *  torch.sum(mesh.cell_volume[0]*gravity_term)

        # Sliding norm and energy
        S = torch.stack((sampling.basal_u[0], sampling.basal_u[1], sampling.basal_u[0] *mesh.bedrock_grad[0]+sampling.basal_u[1] *mesh.bedrock_grad[1]), dim = -1)
        norm_u_bound = 1e-6 + torch.sum(S * S, dim = -1)
        sliding_factor = loss_scale
        sliding = sliding_factor / constants.s*torch.sum(mesh.face_area[0] * constants.C * norm_u_bound**(constants.s/2))

        return viscosity + sliding + gravity