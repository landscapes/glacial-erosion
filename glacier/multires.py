import torch
import torch.nn as nn
import torch.nn.functional as F

from .padding import reflect_pad

"""
Multiresolution operations: up and down sample, pyramiuds, residuals
Uses a gaussian down/upsampling kernel to avoid aliasing
"""


def _gauss_kernel(sigma, window_size):
    X, Y = torch.meshgrid([torch.linspace(-window_size+.5, window_size-.5, 2*window_size)]*2, indexing = 'ij')
    K =  torch.exp(-(X**2+Y**2)/(2*sigma**2)) 
    return K /torch.sum(K)

def _reverse_pad(x, n):
    if n == 0:
        return x
    assert n <= x.shape[2] and n <= x.shape[3]
    x =    torch.cat([torch.flip(x[:, :, :, :n], (3,)), x, torch.flip(x[:, :, :, -n:], (3,))], dim = 3)
    return torch.cat([torch.flip(x[:, :, :n, :], (2,)), x, torch.flip(x[:, :, -n:, :], (2,))], dim = 2)

class OpConv(nn.Module):
    def __init__(self):
        super().__init__()
    def forward(self, x, k):
        k = (k if x.shape[1] == 1 else k.repeat(x.shape[1], 1, 1, 1))
        return F.conv2d(x, k, padding = 0, stride = 2, groups = x.shape[1])

class GDown(nn.Module):
    def __init__(self, sigma, window_size, cyclic_bc = None, op = 'conv'):
        super().__init__()
        self.register_buffer('kernel', _gauss_kernel(sigma, window_size).reshape(1, 1, 2*window_size, 2*window_size))
        self._padding = window_size-1
        self.cyclic_bc = "None" if cyclic_bc is None else cyclic_bc

        if type(op) == str:
            if op == 'conv':
                self.op = OpConv()
            else:
                raise ValueError
        else:
            self.op = op 

    def forward(self, x, n = 1):
        if n == 0:
            return x
        else:
            return self.forward(
                self.op(reflect_pad(x, self.cyclic_bc, self._padding), self.kernel),
                n-1
            )

class GUp(nn.Module):
    def __init__(self, sigma, window_size, cyclic_bc = None):
        super().__init__()

        self.register_buffer('kernel', _gauss_kernel(sigma, window_size).reshape(1, 1, 2*window_size, 2*window_size))
        self.kernel *= 4
        self._padding = window_size-1
        self._prepad = self._padding//2
        self.cyclic_bc = "None" if cyclic_bc is None else cyclic_bc
        if 2*self._prepad !=self._padding:
            self._prepad+=1

    def forward(self, x, n = 1):
        if n == 0:
            return x
        else:
            kernel = (self.kernel if x.shape[1] == 1 else self.kernel.repeat(x.shape[1], 1, 1, 1))
            x = F.conv_transpose2d(reflect_pad(x, self.cyclic_bc, self._prepad), kernel, padding = self._padding, stride = 2, groups = x.shape[1])
            return self.forward(
                x if self._prepad == 0 else x[:, :, 2*self._prepad:-2*self._prepad, 2*self._prepad:-2*self._prepad],
                n-1
            )       

class GSmooth(nn.Module):
    def __init__(self, sigma, window_size, cyclic_bc):
        super().__init__()
        self.down = GDown(sigma, window_size, cyclic_bc)
        self.up   = GUp(sigma, window_size, cyclic_bc)

    def forward(self, x, n = 1):
        return self.up(self.down(x, n), n)

class GPyramidList(nn.Module):
    def __init__(self, sigma, window_size, cyclic_bc, op = 'conv'):
        super().__init__()
        self.down = GDown(sigma, window_size, cyclic_bc, op)

    def forward(self, x, n):

        list_d = [x]
        for _ in range(n):
            x = self.down(x)
            list_d.append(x)

        return list_d

class GPyramid(nn.Module):
    def __init__(self, sigma, window_size, cyclic_bc = None):
        super().__init__()
        self.pyrlist = GPyramidList(sigma, window_size, cyclic_bc)
        self.up   = GUp(sigma, window_size, cyclic_bc)

    def forward(self, x, n):
        plist = self.pyrlist(x, n)
        return [self.up(plist[k], k) for k in range(len(plist))]


class GResidualsList(nn.Module):
    def __init__(self, sigma, window_size, cyclic_bc):
        super().__init__()
        self.pyrlist = GPyramidList(sigma, window_size, cyclic_bc)
        self.up   = GUp(sigma, window_size, cyclic_bc)

    def forward(self, x, n):
        plist = self.pyrlist(x, n) + [None]
        up = lambda x: 0 if x is None else self.up(x)
        return [plist[k] - up(plist[k+1]) for k in range(len(plist)-1)]

class GResiduals(nn.Module):
    def __init__(self, sigma, window_size, cyclic_bc):
        super().__init__()
        self.pyr = GPyramid(sigma, window_size, cyclic_bc)

    def forward(self, x, n):
        plist = self.pyr(x, n) + [torch.zeros_like(x)]
        return [plist[k] - (plist[k+1]) for k in range(len(plist)-1)]