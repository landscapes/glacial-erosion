import torch
import torch.nn.functional as F

from .padding import reflect_pad

def sun_modifier(s, dx, cyclic_bc, sun_dir = None):

    """
    Sun exposure modifier to the mass balance

    params:
        s: surface
        dx
        sun_dir(vector): sun direction. If None is given, the default is [1.0, 2.0, 1.5]

    returns:
        Relative difference to the mean temperature
    """

    s = reflect_pad(s, cyclic_bc)

    # surface gradients
    sx = (s[:, 1:-1, 2:] - s[:, 1:-1, :-2])/2/dx
    sy = (s[:, 2:, 1:-1] - s[:, :-2, 1:-1])/2/dx
    sn = torch.sqrt(sx**2 + sy**2 +1)

    # sun direction
    if sun_dir is None:
        sun_dir = torch.tensor([1.0, 2.0, 1.5]) # default. can stay on the CPU

    # normalize
    sun_dir/= (sun_dir**2).sum().sqrt()

    # exposure. Notes
    # - we reome the "z" term of the dot product, because we only want a relative difference
    return  -sun_dir[0] * sx/sn - sun_dir[1] * sy/sn  #+ sun[2]/sn


def simple_mass_balance(s, ELA, gradabl = .009, gradacc = .005):

    """
    Simple mass balance from an elevation gradient

    params:
        s: surface
        ELA: altitude where M = 0
        gradabl: ablation (M<0) multiplier
        graddacc: accumulatpion (M>0) multiplier

    returns:
        Mass balance
    """

    M = s-ELA
    M *= torch.where(M<0, gradabl, gradacc)

    return M

