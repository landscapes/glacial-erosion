import torch

from .padding import conditional_pad

# helper function for differentials and average operators
dx = lambda u:u[...,       1:] - u[...      , :-1]
dy = lambda u:u[...,    1:, :] - u[...   , :-1, :]
dz = lambda u:u[..., 1:, :, :] - u[... ,:-1, :, :]
avgx = lambda u:(u[...      , 1:] + u[...     , :-1])/2
avgy = lambda u:(u[...   , 1:, :] + u[...  , :-1, :])/2
avgz = lambda u:(u[... ,1:, :, :] + u[...,:-1, :, :])/2
hz_avg = lambda u:avgx(avgy(u))

grad2D = lambda u, _dx : torch.cat((dx(avgy(u)), avgx(dy(u))), dim = 0) / _dx # 1, H, W -> 2, H, W

# Holder class for the sampled values of the velocity and derivatives
class Sampling:
    def __init__(self, cell_u, cell_dudx, cell_dudy, cell_dudz, basal_u):
        self.cell_u, self.cell_dudx, self.cell_dudy, self.cell_dudz, self.basal_u = cell_u, cell_dudx, cell_dudy, cell_dudz, basal_u

    def set_grads(self, g_cell_u, g_cell_dudx, g_cell_dudy, g_cell_dudz, g_basal_u):
        self.g_cell_u, self.g_cell_dudx, self.g_cell_dudy, self.g_cell_dudz, self.g_basal_u = g_cell_u, g_cell_dudx, g_cell_dudy, g_cell_dudz, g_basal_u


class Mesh:
    """
    Definition of the geometry and stroage of the velocity inside the glacier
    """

    def __init__(self, bedrock, surface, dx_, dz_min, num_layers, cyclic_bc, force_min_height = True, eps_min_ice = 1e-5):

        """
        Input:
            bedrock: bedrock altitude,
            surface: ice surface altitude
            dx_: dx
            dz_min: minimum possible vertical difference between two layer
            num_layers: number of layers (including bounds)
            force_min_height: if True and  h is small but not 0, replace h by dz_min
        """

        self.bounds = surface - bedrock < eps_min_ice
        self.bedrock = bedrock
        self.surface = surface
        self.dx = dx_
        self.eps_min_ice = eps_min_ice
        self.cyclic_bc = cyclic_bc
        
        # always assume a minimum height of 'dz_min' for stability 
        stable_surface = torch.where(~self.bounds, torch.maximum(surface, bedrock+(dz_min if force_min_height else 0)), bedrock)

        # compute an even vertical sampling, avoiding gaps smaller than dz_min 
        h = stable_surface-bedrock
        if dz_min > 0:
            self.inter_layers = torch.clamp(torch.round(h/dz_min), 1, num_layers-1)
            self.z = torch.minimum(stable_surface, bedrock + torch.linspace(0, 1, num_layers, device = bedrock.device)[:, None, None]* h * (num_layers-1) / self.inter_layers)[None]
        else:
            self.z = (bedrock + torch.linspace(0, 1, num_layers, device = bedrock.device)[:, None, None]* h)[None]

        self.compute_mesh_data()


    def initial_velocity(self, requires_grad = True):
        """
        Creates initial velocity vector
        """
        return torch.repeat_interleave(torch.zeros_like(self.z), 2, dim = 0).requires_grad_(requires_grad)


    def compute_mesh_data(self):
        """
        Compute mesh peroperties (cell sizes, ...)
        All properties are centered inside the cells / volumes ; with one extra cell on the right / top for cyclic boundary conditions
        """

        z = conditional_pad(self.z, self.cyclic_bc)
        self.z_padded = z

        self.dz_avgxy = avgx(avgy(dz(z)))
        self.empty_cell = self.dz_avgxy < self.eps_min_ice
        self.dz_avgxy_clamped = self.dz_avgxy.clamp(self.eps_min_ice)

        self.face_area = torch.sqrt((self.dx**2 + avgx(dy(z[:, 0]))**2) * (self.dx**2 + avgy(dx(z[:, 0]))**2))
        self.face_area[self.empty_cell[:, 0]] = 0

        
        # self.cell_volume = self.dz_avgxy * avgz(self.face_area)
        self.cell_volume = self.dz_avgxy * self.dx**2

        self.total_volume = torch.sum(self.cell_volume)

        self.bedrock_grad = grad2D(conditional_pad(self.bedrock, self.cyclic_bc), self.dx)
        self.surface_grad = grad2D(conditional_pad(self.surface, self.cyclic_bc), self.dx)


    def _apply_boundary_conditions(self, u, frozen):

        # Frozen boundary conditions
        if hasattr(frozen, 'shape'):
            u = torch.cat([torch.where(frozen, torch.zeros_like(u[:, 0:1]), u[:, 0:1]),
                                u[:, 1:]], dim = 1)
        elif frozen:
            u = torch.cat([torch.zeros_like(u[:, 0:1]), u[:, 1:]], dim = 1)

        # no velocity above glacier (flat cells)
        return torch.cat([u[:, 0:1], 
            torch.where(dz(self.z) < 1e-5, u[:, :-1], u[:, 1:])], dim = 1)

    

    def _sample_cells(self, u):
        """
        Compute values and derivatives
        """

        not_empty = ~self.empty_cell

        cell_u = avgx(avgy(avgz(u)))
        cell_dudz = not_empty * avgx(avgy(dz(u))) / self.dz_avgxy_clamped

        u0, u1, z0, z1 = avgx(avgy(u[:,:-1, :, :])), avgx(avgy(u[:,1:, :, :])), self.z_padded[:,:-1, :, :], self.z_padded[:,1:, :, :]

        # the second term takes into account the deformation of the cells 
        cell_dudx = not_empty*(   dx(avgy(avgz(u))) /self.dx + (u0 * dx(avgy(z1)) - u1 * dx(avgy(z0)) - cell_u*dx(avgy(dz(self.z_padded))))/self.dz_avgxy_clamped/self.dx   )
        cell_dudy = not_empty*(   avgx(dy(avgz(u))) /self.dx + (u0 * avgx(dy(z1)) - u1 * avgx(dy(z0)) - cell_u*avgx(dy(dz(self.z_padded))))/self.dz_avgxy_clamped/self.dx   )

        basal_u = avgx(avgy(u))[:, 0]

        return Sampling(cell_u, cell_dudx, cell_dudy, cell_dudz, basal_u)
    

    def sample_velocity(self, u, frozen):

        u  = self._apply_boundary_conditions(u, frozen)
        return self._sample_cells(conditional_pad(u, self.cyclic_bc))
       


    def ubar(self, u):
        """
        Vertically averaged velocity
        """
        len_z = dz(self.z)
        norm_z = torch.sum(len_z, dim = 1).clamp(self.eps_min_ice)
        return torch.sum(avgz(u)*len_z, dim = 1) / norm_z


    def ubase(self, u):
        """
        basal velocity
        """
        return u[:, 0]
